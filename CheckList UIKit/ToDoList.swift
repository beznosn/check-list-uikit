//
//  ToDoList.swift
//  CheckList UIKit
//
//  Created by Nick Beznos on 2/26/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import Foundation

class ToDoList {
    
   
    
    private var highPriorityToDos: [CheckListItem] = []
    private var mediumPriorityToDos: [CheckListItem] = []
    private var lowPriorityToDos: [CheckListItem] = []
    private var noPriorityToDos: [CheckListItem] = []

    
    init() {
       let row0Item = CheckListItem()
       let row1Item = CheckListItem()
       let row2Item = CheckListItem()
       let row3Item = CheckListItem()
       let row4Item = CheckListItem()
        
        row0Item.text = "Take a jog"
        row1Item.text = "Wathc a movie"
        row2Item.text = "Code an app"
        row3Item.text = "Walk the dog"
        row4Item.text = "Study design patterns"
        
        addTodo(row0Item, for: .high)
        addTodo(row1Item, for: .medium)
        addTodo(row2Item, for: .medium)
        addTodo(row3Item, for: .medium)
        addTodo(row4Item, for: .medium)

    }
    
    func newToDo() -> CheckListItem {
        let item = CheckListItem()
        item.text = getRandomTitle()
        mediumPriorityToDos.append(item)
        return item
    }
    
    func move(item: CheckListItem, from sourcePriority: Priority, at sourceIndex: Int, to destinationPriority: Priority, at destinationIndex: Int) {
        remove(items: [item], from: sourcePriority, at: sourceIndex)
        addTodo(item, for: destinationPriority, at: destinationIndex)
    }
    
    func remove(items: [CheckListItem], from priority: Priority, at index: Int) {
        switch priority {
        case .high:
            highPriorityToDos.remove(at: index)
        case .medium:
            mediumPriorityToDos.remove(at: index)
        case .low:
            lowPriorityToDos.remove(at: index)
        case .no:
            noPriorityToDos.remove(at: index)
        }
    }
    
    func todoList(for priority: Priority) -> [CheckListItem] {
        switch priority {
        case .high:
            return highPriorityToDos
        case .medium:
            return mediumPriorityToDos
        case .low:
            return lowPriorityToDos
        case .no:
            return noPriorityToDos
        }
    }
    
    func addTodo(_ item: CheckListItem, for priority: Priority, at index: Int = -1) {
        switch priority {
        case .high:
            if index < 0 {
            highPriorityToDos.append(item)
            } else {
                highPriorityToDos.insert(item, at: index)
            }
        case .medium:
            if index < 0 {
            mediumPriorityToDos.append(item)
            } else {
                mediumPriorityToDos.insert(item, at: index)
            }
        case .low:
            if index < 0 {
            lowPriorityToDos.append(item)
            } else {
                lowPriorityToDos.insert(item, at: index)
            }
        case .no:
            if index < 0 {
            noPriorityToDos.append(item)
            } else {
                noPriorityToDos.insert(item, at: index)
            }
        }
    }
    
    
    private func getRandomTitle() -> String {
        let titles = ["New todo item", "Generic todo", "Fill me out", "I need something to do", "Much to do about nothing"]
        return titles.randomElement()!
    }
}

enum Priority: Int, CaseIterable {
       case high, medium, low, no
   }
