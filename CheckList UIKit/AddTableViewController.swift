//
//  AddTableViewController.swift
//  CheckList UIKit
//
//  Created by Nick Beznos on 2/26/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import UIKit

protocol ItemDetailVCDelegate: class {
    func addItemViewControllerDidCancel(_ controller: AddTableViewController)
    func addItemViewController(_ controller: AddTableViewController, didFinishAdding item: CheckListItem)
    func addItemViewController(_ controller: AddTableViewController, didFinishEditing item: CheckListItem)
}

class AddTableViewController: UITableViewController {
    weak var delegate: ItemDetailVCDelegate?
    weak var todoList: ToDoList?
    weak var itemToEdit: CheckListItem?
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    @IBAction func Cancel(_ sender: Any) {
        delegate?.addItemViewControllerDidCancel(self)
    }
    
    @IBAction func Done(_ sender: Any) {
        if let item = itemToEdit, let text = textField.text {
            item.text = text
            delegate?.addItemViewController(self, didFinishEditing: item)
        } else {
            if let item = todoList?.newToDo() {
                if let textFieldText = textField.text {
                    item.text = textFieldText
                }
                item.checked = false
                delegate?.addItemViewController(self, didFinishAdding: item)
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        textField.becomeFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let item = itemToEdit {
            title = "Edit"
            textField.text = item.text
            addButton.isEnabled = true
        }
        navigationItem.largeTitleDisplayMode = .never
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return nil
    }

}


extension AddTableViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let oldText = textField.text,
        let stringRange = Range(range, in: oldText) else {
            return false
        }
        
        let newText = oldText.replacingCharacters(in: stringRange, with: string)
        if newText.isEmpty {
            addButton.isEnabled = false
        } else {
            addButton.isEnabled = true
        }
        return true
    }
}
