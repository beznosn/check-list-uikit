//
//  CheckListVC.swift
//  CheckList UIKit
//
//  Created by Nick Beznos on 2/26/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import UIKit

class CheckListVC: UITableViewController {
    var todoList: ToDoList
 
    
    @IBAction func addItem(_ sender: UIBarButtonItem) {
        let newRowIndex = todoList.todoList(for: .medium).count
        _ = todoList.newToDo()
        let indexpath = IndexPath(row: newRowIndex, section: 0)
        tableView.insertRows(at: [indexpath], with: .automatic)
    }
    
    
    @IBAction func deleteItems(_ sender: Any) {
        if let selectedRows = tableView.indexPathsForSelectedRows {
            for indexPath in selectedRows {
                let priority = priorityForSectionIndex(indexPath.section)
                let todos = todoList.todoList(for: priority ?? Priority.medium)
                
                let rowToDelete = indexPath.row > todos.count - 1 ? todos.count - 1 : indexPath.row
                let item = todos[rowToDelete]
                todoList.remove(items: [item], from: priority!, at: rowToDelete)
            }
            tableView.beginUpdates()
            tableView.deleteRows(at: selectedRows, with: .automatic)
            tableView.endUpdates()
        }
    }
    
    
    @IBOutlet weak var deleteButton: UIBarButtonItem!
        
    
    required init?(coder: NSCoder) {
        todoList = ToDoList()
        
        super.init(coder: coder)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.leftBarButtonItem = editButtonItem
        tableView.allowsMultipleSelectionDuringEditing = true
        deleteButton.isEnabled = false
    }
    
    private func priorityForSectionIndex(_ index: Int) -> Priority? {
        return Priority(rawValue: index)
    }
    

    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: true)
        deleteButton.isEnabled = true
        tableView.setEditing(tableView.isEditing, animated: true)
        if !editing {
            deleteButton.isEnabled = false
        }
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let priority = priorityForSectionIndex(section) {
            return todoList.todoList(for: priority).count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckListItem", for: indexPath)
        //let item = todoList.todos[indexPath.row]
        if let priority = priorityForSectionIndex(indexPath.section) {
            let item = todoList.todoList(for: priority)[indexPath.row]
            configureText(for: cell, with: item)
            configureChackMark(for: cell, with: item)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.isEditing {
            return
        }
        if let cell = tableView.cellForRow(at: indexPath) {
            if let priority = priorityForSectionIndex(indexPath.section) {
                let items = todoList.todoList(for: priority)
                let item = items[indexPath.row]
                item.toggleChecked()
                configureChackMark(for: cell, with: item)
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        if let srcPriority = priorityForSectionIndex(sourceIndexPath.section),
            let destPriority = priorityForSectionIndex(destinationIndexPath.section) {

//            if tableView.numberOfRows(inSection: destPriority.rawValue) == 0 {
//                tableView.insertRows(at: [IndexPath(row: 0, section: destPriority.rawValue)], with: .automatic)
//            }
            
            
            let item = todoList.todoList(for: destPriority)[sourceIndexPath.row]
            todoList.move(item: item, from: srcPriority, at: sourceIndexPath.row, to: destPriority, at: destinationIndexPath.row)
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if let priority = priorityForSectionIndex(indexPath.section) {
            let item = todoList.todoList(for: priority)[indexPath.row]
            todoList.remove(items: [item], from: priority, at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    func configureChackMark(for cell: UITableViewCell, with item: CheckListItem) {
        guard let checkmarkCell = cell as? CheckListTableViewCell else {
            return
        }
        if item.checked {
            checkmarkCell.checkmarkLabel.isHidden = false
        } else {
            checkmarkCell.checkmarkLabel.isHidden = true
        }
    }
    
    
    func configureText(for cell: UITableViewCell, with item: CheckListItem) {
        if let checkmarkCell = cell as? CheckListTableViewCell {
            checkmarkCell.todoTextLabel.text = item.text
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddItemSegue" {
            if let addItemVC = segue.destination as? AddTableViewController {
                addItemVC.delegate = self
                addItemVC.todoList = todoList
            }
        } else if segue.identifier == "EditItemSegue" {
            if let addItemVC = segue.destination as? AddTableViewController {
                if let cell = sender as? UITableViewCell, let indexPath = tableView.indexPath(for: cell), let priority = priorityForSectionIndex(indexPath.section) {
                    let item = todoList.todoList(for: priority)[indexPath.row]
                    addItemVC.itemToEdit = item
                    addItemVC.delegate = self
                }
            }
        }
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Priority.allCases.count
    }
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var title: String?
        if let priority = priorityForSectionIndex(section) {
            guard self.tableView(tableView, numberOfRowsInSection: section) != 0 else { return title }
            switch priority {
            case .high:
                title = "High priority"
            case .medium:
                title = "Medium priority"
            case .low:
                title = "Low priority"
            default:
                title = "No priority"
            }
        }
        return title
    }
}

extension CheckListVC: ItemDetailVCDelegate {
    func addItemViewControllerDidCancel(_ controller: AddTableViewController) {
        navigationController?.popViewController(animated: true)
    }
    
    func addItemViewController(_ controller: AddTableViewController, didFinishAdding item: CheckListItem) {
        navigationController?.popViewController(animated: true)
        let rowIndex = todoList.todoList(for: .medium).count - 1
        let indexPath = IndexPath(row: rowIndex, section: Priority.medium.rawValue)
        tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    func addItemViewController(_ controller: AddTableViewController, didFinishEditing item: CheckListItem) {
        for priority in Priority.allCases {
            
            let currentList = todoList.todoList(for: priority)
            if let index = currentList.firstIndex(of: item) {
                let indexPath = IndexPath(row: index, section: priority.rawValue)
                if let cell = tableView.cellForRow(at: indexPath) {
                    configureText(for: cell, with: item)
                }
            }
        }
        navigationController?.popViewController(animated: true)
    }
    
}
