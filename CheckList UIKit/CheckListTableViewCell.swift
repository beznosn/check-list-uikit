//
//  CheckListTableViewCell.swift
//  CheckList UIKit
//
//  Created by Nick Beznos on 2/27/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import UIKit

class CheckListTableViewCell: UITableViewCell {
    @IBOutlet weak var checkmarkLabel: UILabel!
    @IBOutlet weak var todoTextLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
