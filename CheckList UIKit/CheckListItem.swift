//
//  CheckListItem.swift
//  CheckList UIKit
//
//  Created by Nick Beznos on 2/26/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import Foundation

class CheckListItem: NSObject {
    @objc var text = ""
    var checked = false
    
    func toggleChecked() {
        checked = !checked
    }
}
